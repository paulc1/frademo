# pymoku example: Plotting Frequency Response Analyzer
#
# This example demonstrates how you can generate output sweeps using the
# Frequency Response Analyzer instrument, and view transfer function data in
# real-time.  X,Y limits are set for an upper passing template. 
#
# (c) 2021 Liquid Instruments Pty. Ltd.
#

from pymoku import Moku
from pymoku.instruments import FrequencyResponseAnalyzer

import matplotlib.pyplot as plt
import numpy as np

# Connect to your Moku by its device name
# Alternatively, use Moku.get_by_serial('#####') or Moku('192.168.###.###')
m = Moku.get_by_name('Moku silver')

# Define output sweep parameters here for readability
f_start = 10e6 # Hz
f_end = 34e6 # Hz
sweep_length = 512
log_scale = False
single_sweep = False
amp_ch1 = 1.0 # Vpp
amp_ch2 = 0.5 # Vpp
averaging_time = 1e-3 # sec
settling_time = 1e-3 # sec
averaging_cycles = 1
settling_cycles = 1

try:
 # See whether there's already a Frequency Response Analyzer running. If
 # there is, take control of it; if not, deploy a new one.
 i = m.deploy_or_connect(FrequencyResponseAnalyzer)

 # Many PCs struggle to plot magnitude and phase for both channels at the
 # default 10fps, turn it down so it remains smooth, albeit slow. Turn the
 # output to 'sweep' mode so we can see the in-progress sweep (set to
 # 'full_frame' or leave blank if if you only want to get completed traces,
 # e.g. for analysis rather than viewing)
 i.set_framerate(5)
 i.set_xmode('sweep')

 # Set the output sweep amplitudes
 i.set_output(1, amp_ch1)
 i.set_output(2, amp_ch2)

 # Set the sweep configuration
 i.set_sweep(f_start, f_end, sweep_length, log_scale, averaging_time,
 settling_time, averaging_cycles, settling_cycles)

 # Start the output sweep in loop mode
 i.start_sweep(single=single_sweep)

 # Set up the amplitude plot
 plt.subplot(211)
 if log_scale:
 # Plot log x-axis if frequency sweep scale is logarithmic
   mag_line, = plt.semilogx([])
   upperlimit_line, = plt.semilogx([])
   fail_line, = plt.semilogx([], color='red')
 else:
   mag_line, = plt.plot([])
   upperlimit_line, = plt.plot([])
   fail_line, = plt.plot([], color='red')


 ax_1 = plt.gca()
 ax_1.set_xlabel('Frequency (Hz)')
 ax_1.set_ylabel('Magnitude (dB)')
 plt.title("Moku frequency response (blue) with limit (orange)")

 # Set up the phase plot
 plt.subplot(212)
 if log_scale:
  phase_line, = plt.semilogx([])
 else:
  phase_line, = plt.plot([])




 ax_2 = plt.gca()
 ax_2.set_xlabel('Frequency (Hz)')
 ax_2.set_ylabel('Phase (Cycles)')

 plt.ion()
 plt.show()
 plt.grid(b=True)


 # setup the limit matrix
 # xlimits are in Hz, ylimits in dB
 xlimits = [10e6, 15e6, 17e6, 28e6, 29e6, 30e6, 34e6]
 ylimits = [-20, -31, -0.75, -0.75,-35, -25, -26]

 print(str(np.interp([138e5, 15e6], xlimits, ylimits)))

 
 # Retrieves and plot new data
 while True:
   frame = i.get_realtime_data(timeout=None, wait=True)

   upper_limit = np.interp(frame.frequency, xlimits, ylimits)

   measured = np.array(frame.ch1.magnitude_dB)

   plt.subplot(211)


   passfail = [0] * 512
   topfill = [0] * 512

   if not None in measured:
     passfail = np.select([measured<=upper_limit],[0], -20)


   
   # Set the frame data for 1 channel plot
   mag_line.set_ydata(frame.ch1.magnitude_dB)
   mag_line.set_xdata(frame.frequency)
   
   upperlimit_line.set_ydata(ylimits)
   upperlimit_line.set_xdata(xlimits)

   fail_line.set_ydata(passfail)
   fail_line.set_xdata(frame.frequency)

   fail_bar = plt.bar(x=frame.frequency, height=passfail)


   # Phase
   plt.subplot(212)
   if not None in frame.ch1.phase:
     phase = np.unwrap(frame.ch1.phase, period = 0.5)
   else:
   	phase = frame.ch1.phase

   phase_line.set_ydata(phase)
   phase_line.set_xdata(frame.frequency)

   # Ensure the frequency axis is a tight fit
   ax_1.set_xlim(min(frame.frequency), max(frame.frequency))
   ax_2.set_xlim(min(frame.frequency), max(frame.frequency))
   ax_1.relim()

   ax_1.autoscale_view()
   ax_2.relim()
   ax_2.autoscale_view()

   # ax_1.fill_between(frame.frequency, passfail,topfill, color='red')


   # Redraw the lines
   plt.draw()
   plt.pause(0.001)
finally:
    m.close()
 